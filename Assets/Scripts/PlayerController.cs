﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float vehicleSpeed = 10f;
    [SerializeField] float turnspeed = 10f;
    private Rigidbody rb;
    private float horizontalInput;
    private float verticalInput;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        

    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        Debug.Log("Found rigidbody on " + GetComponent<Rigidbody>().gameObject.name);

        //move vehicle forward
        transform.Translate(Vector3.forward * Time.deltaTime * vehicleSpeed * verticalInput);
        //move vehicle left/right
        //transform.Translate(Vector3.right * Time.deltaTime * turnspeed * horizontalInput);
        //rotate vehicle left/right along the up axis
        transform.Rotate(Vector3.up, turnspeed * horizontalInput * Time.deltaTime);
    }
}
